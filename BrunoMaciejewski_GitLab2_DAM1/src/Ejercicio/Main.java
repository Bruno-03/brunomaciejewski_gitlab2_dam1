package Ejercicio;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        int opcion=0;
        // while dura hasta que se seleccione la cuarta opción
        while (opcion!=4){ 
            System.out.println("Elige una opción. Opción 1: Cambio de unidades de horas a segundos\n"
                + "Opción 2: Cambio de unidades de kilómetros a metros\n"
                + "Opción 3: Cambio de unidades de km/h a m/s\n"
                + "Opción 4: Salir");
            opcion = read.nextInt();
            switch (opcion) {
            // Pide por pantalla las horas y luego las escribe convirtiendolas a segundos
            case 1:
                System.out.println("Escribe las horas");
                int horas=read.nextInt();
                System.out.println("Los segundos son "+horas*3600);
                break;
            // Pide por pantalla los kilometros y luego escribe la conversión a metros
            case 2:
                System.out.println("Escribe los kilometros");
                int kilometros=read.nextInt();
                System.out.println("Los metros son: "+kilometros*1000);
                break;
            // Pide por pantalla kilometros por hora y luego escribe la conversión a metros por segundo
            case 3:
                System.out.println("Escribe los km/h");
                double kmh=read.nextDouble();
                System.out.printf("Los m/s son: %.2f %n", kmh/3.6);
                break;
            // Sale del bucle while
            case 4:
                System.out.println("Saliendo...");
                break;
            // Un error que ocurre cuando la opción introducida no se corresponde con ninguna opción válida
            default:
                System.out.println("Opción no válida");
            }
            System.out.println("================");
        }
    }
}
